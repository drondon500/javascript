var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Quieres imprimir un numero?, escribelo aqui: ", function(answer) {
  console.log("Aqui esta tu numero", answer);

  rl.close();
});